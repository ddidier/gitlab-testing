# GitLab Test

Test the features of GitLab.

## Issues

### Issue Labels

See [GitLab Test > Issues > Labels](https://gitlab.com/ddidier/gitlab-test/-/labels).

Use `Generate a default set of labels` then add `ci/cd` (`#8e44ad`) and `feature` (`#5cb85c`).
